// <h2_66>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define FUNC leftmost_one
const int g_W = 0x20; // 32 bits

int leftmost_one(unsigned x)
{
	// corrupting right bits
	x |= x >> 0x01;
	x |= x >> 0x02;
	x |= x >> 0x04;
	x |= x >> 0x08;
	x |= x >> 0x10;

	// erasing right bits
	return x ^ x >> 1;

	// 12 operations used
}

int rival_func(unsigned x)
{
	for (int i2 = 0; i2 < g_W; i2++)
	{
		if (1 == ( x >> i2 ))
			return 1 << i2;
	}
	
	return 0;
}

int test(int i)
{
	return rival_func(i) == FUNC(i);
}

void print_example()
{
	unsigned testCase[] = { 0xff00, 0x6600 };
	for (int i2 = 0; i2 < ARRLEN(testCase); i2++)
	{
		printf("%#x -> %#x\n", testCase[i2], FUNC(testCase[i2]));
	}
}

int main(void)
{
	srand((unsigned)time(NULL));

	{
		int i1;
		for (i1 = 0; ; )
		{
			if (!test(rand() * rand() * rand()))
			{
				printf("Fail\n");

				break;
			}

			if (0x1000 == ++i1)
			{
				printf("Pass\n");

				break;
			}
		}
	}

	print_example();

	return 0;
}

// </h2_66>
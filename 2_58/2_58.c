// <h2_58>

#include <stdio.h>


#define LENGTH(x) ( sizeof(x) / sizeof(*(x)) )

#define BYTE2BITS(x) ( (x) << 3 )

typedef int WORD;


int is_little_endian()
{
	char big_endian_format[sizeof(WORD)];
	for (int i2 = 0; i2 < LENGTH(big_endian_format); i2++)
	{
		big_endian_format[i2] = i2;
	}

	WORD check = *(WORD*)big_endian_format;
	int i0, acc0;

	// Your system is a little-endian.
	for (i0 = 0, acc0 = check; i0 < sizeof(WORD) && big_endian_format[i0] == ( 0xFF & acc0 ); i0++, acc0 >>= BYTE2BITS(1));
	if (sizeof(WORD) <= i0)
		return 1;

	// Your system is a big-endian.
	for (i0 = sizeof(WORD) - 1, acc0 = check; i0 >= 0 && big_endian_format[i0] == ( 0xFF & acc0 ); i0++, acc0 >>= BYTE2BITS(1));
	if (sizeof(WORD) <= i0)
		return 0;

	// The other cases.
	return -1;
}

int main(void)
{
	printf("%d\n", is_little_endian());

	return 0;
}

// </h2_58>
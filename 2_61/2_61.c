// <h2_61>

#include <stdio.h>
#include <limits.h>


#define LENGTH(x) ( sizeof(x) / sizeof(*(x)) )

#define BYTE2BITS(x) ( (x) << 3 )

typedef int QTYPE;
#define QTYPE_MAX INT_MAX
#define QTYPE_MIN INT_MIN


int areAllBits1(QTYPE q)
{
	return !~q;
}

int areAllBits0(QTYPE q)
{
	return !q;
}

int areAllBitsOfLSByte1(QTYPE q)
{
	return !(0xFF & ~q);
}

int areAllBitsOfMSByte0(QTYPE q)
{
	return !( 0xFF << BYTE2BITS(sizeof(QTYPE) - 1) & q );
}

int main()
{
	printf("A: %d %d %d\n", areAllBits1(-1), areAllBits1(0x00FF00FF), areAllBits1(0));
	printf("B: %d %d %d\n", areAllBits0(0), areAllBits0(0x00FF00FF), areAllBits0(-1));
	printf("C: %d %d %d\n", areAllBitsOfLSByte1(0x000000FF), areAllBitsOfLSByte1(0xFFFFFF00), areAllBitsOfLSByte1(0x00000001));
	printf("D: %d %d %d\n", areAllBitsOfMSByte0(0x00FFFFFF), areAllBitsOfMSByte0(0x01000000), areAllBitsOfMSByte0(0xFF000000));

	return 0;
}

// </h2_61>
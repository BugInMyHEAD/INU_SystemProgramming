// <h2_80>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define FUNC threefourths
const int g_W = 0x20; // 32 bits

int threefourths(int x)
{
	int negFlag = !!( INT_MIN & x );
	int bias = negFlag | negFlag << 1;
	int q = x >> 2;
	int r = 3 & x;

	return q + q + q + ((r + r + r + bias) >> 2);
}

int rival_func(int x)
{
	return (int)( 0.75 * x );
}

int test(int i)
{
	return rival_func(i) == FUNC(i);
}

void print_example()
{
	int testCase[] = { 8, 7, 5, 4, 3, -8, -7, -5, -4 , -3 };
	for (int i2 = 0; i2 < ARRLEN(testCase); i2++)
	{
		printf("%d -> %d\n", testCase[i2], FUNC(testCase[i2]));
	}
}

int main(void)
{
	srand((unsigned)time(NULL));

	{
		int i1;
		for (i1 = 0; ; )
		{
			if (!test(rand() * rand() * rand()))
			{
				printf("Fail\n");

				break;
			}

			if (0x1000 == ++i1)
			{
				printf("Pass\n");

				break;
			}
		}
	}

	print_example();

	return 0;
}

// </h2_80>
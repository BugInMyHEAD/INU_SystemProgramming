// <h2_67>

/* A.
Bit shift operation beyond its word size is undefined.
Some systems fill 0s, others use modular to rvalue.
B. & C. following code
*/

#include <stdio.h>
#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define FUNC int_size_is_32
const int g_W = 0x20; // 32 bits

int int_size_is_32(void)
{
	/* Set most significant bit (msb) of 32-bit machine */

	// int set_msb = 1 << 31; // solving problem B.

	int set_msb = 1 << 15; // solving problem B. and C. 
	set_msb <<= 15;        // simultaneously
	set_msb <<= 1;         //

	/* Shift past msb of 32-bit word */
	int beyond_msb = set_msb << 1;

	/* set_msb is nonzero when word size >= 32
	   beyond_msb is zero when word size <= 32 */
	return set_msb && !beyond_msb;
}

int main(void)
{
	printf("%d\n", int_size_is_32());

	return 0;
}

// </h2_67>
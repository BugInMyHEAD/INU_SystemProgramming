// <h2_72>

/* A. size_t type is defined like this in MSVC.

	// Definitions of common types
	#ifdef _WIN64
	typedef unsigned __int64 size_t;
	#else
	typedef unsigned int     size_t;
	#endif

The operation between the same size variables, 
the signed and the unsigned always returns 
an unsigned result which is >= 0. 
And, unsigned - unsigned = unsigned >= 0.. 

B. The following code, ...
*/

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define FUNC copy_int
const int g_W = 0x20; // 32 bits

/* (B.) That the type of maxbyte is size_t makes it
more clear that it is about memory and size */
/* copy integer into buffer if space is available */
void copy_int(int val, void* buf, size_t maxbytes)
{
	if (sizeof(val) <= maxbytes)
		memcpy(buf, &val, sizeof(val));
}

void print_example()
{
	int testCase[] = { 0, 0, 0, 0, 0, 0 };
	int val = 1;
	copy_int(val, &testCase[1], sizeof(val));
	copy_int(val, &testCase[4], sizeof(val) - 1);
	for (int i2 = 0; i2 < ARRLEN(testCase); i2++)
	{
		printf("%d\n", testCase[i2]);
	}
}

int main(void)
{
	print_example();

	return 0;
}

// </h2_72>
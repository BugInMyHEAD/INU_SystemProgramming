// <h2_62>

#include <stdio.h>
#include <limits.h>


#define LENGTH(x) ( sizeof(x) / sizeof(*(x)) )

#define BYTE2BITS(x) ( (x) << 3 )

typedef int QTYPE;
#define QTYPE_MAX INT_MAX
#define QTYPE_MIN INT_MIN


int int_shifts_are_arithmetic(void)
{
	/* { return -1 >> BYTE2BITS(sizeof(int)); } produces a warning message(undefined behavior) */
	return -1 >> BYTE2BITS(sizeof(int) - 1) >> BYTE2BITS(1);
}

int main(void)
{
	printf("%d\n", int_shifts_are_arithmetic());

	return 0;
}

// </h2_62>
#include <stdio.h>
#include <string.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int good_echo(void)
{
	int overflow = 0;

	for (;;)
	{
		char buf[12];
		fgets(buf, ARRLEN(buf), stdin);
		printf("%s", buf);
		size_t actualLen = strlen(buf);
		if(!f(stdin))
		{
			overflow = 1;
		}
		else
		{
			break;
		}
	}

	return overflow;
}

int main()
{
	int overflow = good_echo();
	printf("%d\n", overflow);

	return 0;
}

// <h2_73>

#include <stdio.h>
#include <limits.h>
#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define FUNC saturating_add
const int g_W = 0x20; // 32 bits

/* addition that saturates to TMin or TMax */
int saturating_add(int x, int y)
{
	int posFlag = !(INT_MIN & x) && !(INT_MIN & y);
	int negFlag = (INT_MIN & x) && (INT_MIN & y);
	int posInf = INT_MAX;
	int negInf = INT_MIN;
	int addRes = x + y;
	int resNegFlag = !!(INT_MIN & addRes);
	int bitmask = resNegFlag;
	bitmask <<= 0x01;
	bitmask <<= 0x02;
	bitmask <<= 0x04;
	bitmask <<= 0x08;
	bitmask <<= 0x10;

	return bitmask | addRes;
}

void print_example()
{
	printf("%d", saturating_add(INT_MAX, INT_MAX));
}

int main(void)
{
	print_example();

	return 0;
}

// </h2_73>